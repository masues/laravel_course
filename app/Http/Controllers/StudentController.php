<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentController extends Controller
{
  public function index(){
    $students = Student::paginate(5);
    return view('welcome',compact('students'));
  }

  public function create(){
    return view('create');
  }

  public function store(Request $request){
    //valida que los datos esten presentes
    $this->validate($request,[
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required',
        'phone' => 'required',
    ]);
    $student = new Student; //Crea un nuevo estudiante
    $student->first_name = $request->first_name;
    $student->last_name = $request->last_name;
    $student->email = $request->email;
    $student->phone = $request->phone;
    $student->save(); //Almacena al estudiante en la BD
    return redirect(route('home'))->
      with('successMsg','Student successfully added 👍');
  }

  public function edit($id){
    //Encuentra al estudiante con el id especificado
    $student = Student::find($id);
    if(!empty($student)){
      //Si lo encuentra, retorna a la vista
      //Pasa a la vista a la variable student
      return view('edit', compact('student'));
    }
    else{
      //Si no encuentra al estudiante envia un mensaje de error
      return redirect(route('home'))->
      with('errorMsg','Student with id='.$id
        .' wasn\'t found in the database 😟');
    }
  }

  public function update(Request $request, $id){
    //valida que los datos esten presentes
    $this->validate($request,[
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required',
        'phone' => 'required',
    ]);
    //Encuentra al estudiante con el id especificado
    $student = Student::find($id);
    if(!empty($student)){
      $student->first_name = $request->first_name;
      $student->last_name = $request->last_name;
      $student->email = $request->email;
      $student->phone = $request->phone;
      $student->save(); //Almacena al estudiante en la BD
      return redirect(route('home'))->
        with('successMsg','Student successfully updated 👍');
    }
    else{
      //Si no encuentra al estudiante envia un mensaje de error
      return redirect(route('home'))->
      with('errorMsg','Student with id='.$id
        .' wasn\'t found in the database 😟');
    }
  }

  public function delete($id){
    //Encuentra al estudiante con su id
    $student = Student::find($id);
    if(!empty($student)){
      $student->delete();
      return redirect(route('home'))->with('successMsg','Student with id='
        .$id.' was successfully deleted from the database ☠'); 
    }
  }
}
