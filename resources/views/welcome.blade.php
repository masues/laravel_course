@extends('layouts.main')

@section('content')
<div class="container">
  <h1> Home Page </h1>

  {{-- Impresión de mensajes en caso de existir --}}
  @if (session('successMsg'))
  <div class="alert alert-success" role="alert">
    {{session('successMsg')}}
  </div>
  @endif
  @if (session('errorMsg'))
  <div class="alert alert-danger" role="alert">
    {{session('errorMsg')}}
  </div>
  @endif


  {{-- Dibujo de tabla para mostrar datos --}}

  <table class="table">
    <thead class="black white-text">
      <tr>
        <th scope="col">#</th>
        <th scope="col">First name</th>
        <th scope="col">Last name</th>
        <th scope="col">Email</th>
        <th scope="col">Phone</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($students as $student)
      <tr>
        <th scope="row">{{$student->id}}</th>
        <td>{{$student->first_name}}</td>
        <td>{{$student->last_name}}</td>
        <td>{{$student->email}}</td>
        <td>{{$student->phone}}</td>
        <td>

          {{-- Edit button --}}
          <a class="btn btn-raised btn-primary btn-sm"
            href="{{ route('edit', $student->id) }}"> 
            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
          </a>


          {{-- Delete button --}}
          <form method="POST" action={{ route('delete',$student->id) }}
            style="display: inline" id="delete-form-{{ $student->id }}">
             {{-- Función para verificar que la petición es legitima --}}
            {{ csrf_field() }}
            {{-- Función para soportar al método delete dentro de HTML--}}
            {{ method_field('DELETE') }}

            <button class="btn btn-raised btn-danger btn-sm" 
              onclick="delete_student({{$student->id}})">
              <i class="fa fa-trash" aria-hidden="true"></i>
            </button>
            
            <script>
              /*
              Javacript que recibe como parametro al id del estudiante
              y pregunta si de verdad desea eliminarlo
              */
              function delete_student(student_id) {
                if(confirm("Do you really want to delete the student with id="
                  +student_id+"? 😱")){
                  event.preventDefault();
                  document.getElementById("delete-form-"+student_id)
                    .submit(); 
                }
                else{
                  event.preventDefault();
                }
              }
            </script>

          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  {{-- Para agregar una paginación basta con llamar al método links --}}
  {{ $students->links() }}
</div>
@endsection
